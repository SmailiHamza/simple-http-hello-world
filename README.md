# Simple HTTP Hello World

Ce projet démarre un serveur HTTP basique qui répond aux requêtes HTTP sur le endpoint **/hello**.

Le serveur écoute sur le port **8181**. 

## Build du projet

Avec maven : `mvn package`. 

Le jar généré est disponible dans /target. Un jar auto-exécutable avec les dépendances du projet est disponible (sous le nom `*-jar-with-dependencies.jar`).

## Exemple

```
# Request
GET /hello

# Response
200 

Hello, world! It is currently 20:20:02.020
``` 

## Build de l'image Docker

Pour builder L'image Docker de ce projet :
    - Ouvrir un terminal
    - Exécuter la commande 'docker build -t hello-world . ' cela va créer une image docker avec le nom hello-world
    - Pour lancer le container exécuter la commande 'docker run -p 8080:8080 hello-world --name hello'
